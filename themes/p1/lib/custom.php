<?php
/**
 * Custom functions, all this is optional
 * Mosly cleaning up the admin interface
 */



//
//		Adds foundations flex video container around oembed embeds
//
//////////////////////////////////////////////////////////////////////


add_filter('embed_oembed_html', 'embed_oembed', 99, 4);
function embed_oembed($html, $url, $attr, $post_id) {
  return '<div class="flex-video">' . $html . '</div>';
}







//
//		Fixes overlapping adminbar for Foundations top-bar
//
//////////////////////////////////////////////////////////////////////


add_action('wp_head', 'admin_bar_fix', 5);
function admin_bar_fix() {
  if( is_admin_bar_showing() ) {
    $output  = '<style type="text/css">'."\n\t";
    $output .= '@media screen and (max-width: 600px) {#wpadminbar { position: fixed !important; } }'."\n";
    $output .= '</style>'."\n";
    echo $output;
  }
}







//
//		Adds Foundation classes to next/prev buttons
//
//////////////////////////////////////////////////////////////////////


add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="button tiny"';
}













//
//    Adds the livereload script. Primarily for testing other devices on same network as web server
//    Change the IP address to the IP of the computer thats running the "gulp" command (likely your dev computer)
//
//////////////////////////////////////////////////////////////////////


function livereload() {
  wp_register_script('livereload', 'http://192.168.1.110:35729/livereload.js?snipver=1', array(), null, true);
  wp_enqueue_script('livereload');
}

// Run the livereload function if domain contains .dev
$host = $_SERVER['HTTP_HOST'];
if (strpos($host,'.dev') !== false) {
 add_action('wp_enqueue_scripts', 'livereload');
}








//
//		Removes default dashboard widgets
//
//////////////////////////////////////////////////////////////////////



function remove_dashboard_widgets() {
    global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	update_user_meta( get_current_user_id(), 'show_welcome_panel', false );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );







//
//		Removes meta boxes from post
//
//////////////////////////////////////////////////////////////////////



function remove_meta_boxes() {
	// remove_meta_box( 'pageparentdiv' , 'page', 'normal');
	remove_meta_box( 'tagsdiv-post_tag', 'post', 'normal');
	remove_meta_box( 'categorydiv', 'post', 'normal');
}
add_action('admin_menu', 'remove_meta_boxes');










//
//		Removes comments menu
//
//////////////////////////////////////////////////////////////////////


function remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );













//
//		Removes Types (custom post type generator) marketing
//
//////////////////////////////////////////////////////////////////////


// function adminstyle() {
//    echo '<style type="text/css">
//            #wpcf-marketing { display: none;}
//          </style>';
// }
// add_action('admin_head', 'adminstyle');







function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Kunngjøring';
    $submenu['edit.php'][5][0] = 'Kunngjøring';
    $submenu['edit.php'][10][0] = 'Ny Kunngjøring';
    $submenu['edit.php'][16][0] = 'Kunngjøring Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Kunngjøring';
    $labels->singular_name = 'Kunngjøring';
    $labels->add_new = 'Ny Kunngjøring';
    $labels->add_new_item = 'Ny Kunngjøring';
    $labels->edit_item = 'Endre Kunngjøring';
    $labels->new_item = 'Kunngjøring';
    $labels->view_item = 'Vis Kunngjøring';
    $labels->search_items = 'Søk Kunngjøring';
    $labels->not_found = 'No Kunngjøring found';
    $labels->not_found_in_trash = 'No Kunngjøring found in Trash';
    $labels->all_items = 'Alle Kunngjøringer';
    $labels->menu_name = 'Kunngjøring';
    $labels->name_admin_bar = 'Kunngjøring';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );









// Søkeboks på kontakt oss
function customize_output($results , $arg, $id, $getdata ){
     // The Query
    $apiclass = new uwpqsfprocess();
    $query = new WP_Query( $arg );
    ob_start();    $result = '';

    // The Loop
    if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post(); ?>



        <div class="medium-6 large-4 columns">
            <div class="ansatt clearfix">
                <div class="avatar"><?php echo get_the_post_thumbnail(get_the_ID(), 'thumbnail'); ?></div>
                <h1 class="navn"><?php the_title(); ?></h1>
                <div class="stilling"><?php $tmp = get_post_meta(get_the_ID(), 'stilling'); echo $tmp[0]; ?>, <?php $tmp = get_post_meta(get_the_ID(), 'avdeling'); $tmp = $tmp[0]['post_title']; echo str_replace(array('Avdeling', 'Hovedkontor'), '', $tmp); ?></div>
                <div class="links">
                    <?php if(is_mobile()): ?>
                        <a href="tel:+47<?php $tmp = get_post_meta( get_the_ID(), 'telefonnummer'); $tmp = $tmp[0]; echo str_replace(' ', '', $tmp); ?>"><?php $tmp = get_post_meta( get_the_ID(), 'telefonnummer'); echo $tmp[0]; ?></a>
                    <?php else: ?>
                        <span><?php $tmp = get_post_meta( get_the_ID(), 'telefonnummer'); echo $tmp[0]; ?></span>
                    <?php endif; ?>
                    — <a href="mailto:<?php $tmp = get_post_meta( get_the_ID(), 'epost'); echo $tmp[0]; ?>"><?php $tmp = get_post_meta( get_the_ID(), 'epost'); echo $tmp[0]; ?></a>
                </div>
            </div>
        </div>



    <?php }

        echo  $apiclass->ajax_pagination($arg['paged'],$query->max_num_pages, 4, $id, $getdata);

    } else {

        echo  'Fant ingen ved det navnet. Stavet riktig?';

    }
    /* Restore original Post Data */
    wp_reset_postdata();

    $results = ob_get_clean();
    return $results;
}
add_filter('uwpqsf_result_tempt', 'customize_output', '', 4);













if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'prosjekt', 1920, 600, true ); //(cropped)
    add_image_size( 'prosjekt_small', 1400, 520, true ); //(cropped)
    add_image_size( 'prosjekt_list_big', 930, 1000, true ); //(cropped)
}


add_filter('image_resize_dimensions', 'filterCompress', 1, 6);

function filterCompress($foo, $orig_w, $orig_h, $dest_w, $dest_h, $crop) {
  if ( $orig_w == $dest_w && $orig_h == $dest_h ) {
        return array( 0, 0, 0, 0, (int) $orig_w, (int) $orig_h, (int) $orig_w, (int) $orig_h );
  }
  return null;
}








add_filter('user_trailingslashit', 'no_trailing_slash_on_shop');
function no_trailing_slash_on_shop($url_path) {
  if ($url_path=='/tjenester/')
    $url_path = '/tjenester';
  return $url_path;
}