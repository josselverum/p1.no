
<?php if (!have_posts()) : ?>
  <p><?php _e('Sorry, no results were found.', 'roots'); ?> </p>
  <?php get_search_form(); ?>
<?php endif; ?>


<div class="archive-prosjekt clearfix">
	<?php while (have_posts()) : the_post(); ?>
		<div class="projektwrap"><?php get_template_part( 'templates/prosjekt-list-item' ); ?></div>
	<?php endwhile; ?>
</div>