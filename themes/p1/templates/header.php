<span itemscope itemtype="http://schema.org/LocalBusiness"> <!-- Google -->


<div class="hide">
  <h1><span itemprop="name">Plan1</span> er et tverrfaglig planleggings-, rådgivnings- og prosjekteringsfirma innenfor fagfeltene arkitektur, byggeteknikk, interiør, arealplanlegging, landskapsarkitektur og prosjektadministrasjon.</h1>
</div>


<!-- Google -->






<div id="menu-modal" class="reveal-modal full" data-reveal>

  <?php if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(array('theme_location' => 'primary_navigation'));
  endif; ?>

    <span class="p1-divider"></span>
    
    <div class="contact">
      <?php if(is_mobile()): ?>
        <a href="tel:+47<?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>"><?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?></a>
      <?php else: ?>
        <?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>
      <?php endif; ?>
      <a href="mailto:<?php echo pods('instillinger_for_kontaktinfo')->field('hovedepost'); ?>"><?php echo pods('instillinger_for_kontaktinfo')->field('hovedepost'); ?></a>
    </div>

  <a class="close-reveal-modal">&#215;</a>
</div>





<div class="head-wrap">
  <header class="top">
    <div class="row">
      <div class="columns">
        <h1 class="left"><a href="<?php echo home_url(); ?>/">
          <?php if(is_front_page()): ?>
            <img data-interchange="[<?php bloginfo('template_directory'); ?>/assets/img_min/logo-tall-white.svg, (default)], [<?php bloginfo('template_directory'); ?>/assets/img_min/logo-wide-white.svg, (large)]">
          <?php else: ?>
            <img itemprop="image" src="<?php bloginfo('template_directory'); ?>/assets/img_min/logo-wide-white.svg" alt="<?php bloginfo('name') ?>">
          <?php endif; ?>
        </a></h1>
        <nav class="tab-bar right hide-for-large-up"><a class="menu-icon" data-reveal-id="menu-modal" href="#"><span></span></a></nav>
        <div class="show-for-large-up main-nav right"><?php if (has_nav_menu('primary_navigation')) : wp_nav_menu(array('theme_location' => 'primary_navigation')); endif; ?></div>
      </div>
    </div>
  </header>




<?php if(is_page('om-plan1') or (is_page('kontakt')) or is_front_page()): ?>
  <div class="header-page-info">
      <div class="row">
        <div class="columns">
            <?php if(is_page('om-plan1')): ?>
              <div class="omplan1heading" <?php if(!(is_mobile() or is_tablet())): ?> data-0="margin-top: 0px; opacity: 1" data-300="margin-top: -300px; opacity: 0" <?php endif; ?>>
                <?php $side = new WP_Query("page_id=154"); while($side->have_posts()) : $side->the_post();?><?php the_content(); ?><?php endwhile; wp_reset_postdata(); ?>
                <p class="orgnr">Plan1s organisasjonsnummer er <?php echo pods('instillinger_for_kontaktinfo')->field('organisasjonsnummer'); ?></p>
              </div>
            <?php endif; ?>  
            <?php if(is_page('kontakt')): ?>
              <span class="ring-oss-label text-center">Ring oss!</span>
              <h1 class="hovednummer">
                <?php if(is_mobile()): ?>
                  <a href="tel:+47<?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>"><?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?></a>
                <?php else: ?>
                  <?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>
                <?php endif; ?>
              </h1>
            <?php endif; ?>
            <?php if(is_front_page()): ?>
              <ol class="fagomrader" <?php if(!(is_mobile() or is_tablet())): ?> data-0="margin-left: 0px; opacity: 1" data-600="margin-left: -300px; opacity: 0" <?php endif; ?>>
                <?php $args = array( 'post_type' => 'fagomrade', 'posts_per_page' => 10 ); $loop = new WP_Query( $args ); while ( $loop->have_posts() ) : $loop->the_post(); ?>
                  <li><a href="/tjenester#<?php global $post; echo $post->post_name; ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
              </ol>
              <div class="text-center show-for-small-only"><span class="p1-divider"></span></div>
            <?php endif; ?>
        </div>
      </div>
    </div>
<?php endif; ?>


<?php if ( is_singular(array('prosjekt', 'fagomrade'))): ?>
  <div class="header-page-image">
    <img 
          data-interchange="[<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); echo $tmp[0]; ?>, (default)],
                    [<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'prosjekt_small' ); echo $tmp[0]; ?>, (medium)]
      ">
  </div>
<?php endif; ?>







<?php if(is_page('kontakt')): ?>
      <div class="avdelinger">
      <div class="row">
        
          <?php 

          $args = array( 'post_type' => 'avdeling', 'order_by' => '' );
          $loop = new WP_Query( $args );
          while ( $loop->have_posts() ) : $loop->the_post(); ?>
            
            <div class="avdeling medium-4 columns">
              <div class="avdelingen">
                <h1><?php the_title(); ?></h1>
                <address>
                  <span class="adresse"><?php $tmp = get_post_meta($post->ID, 'adresse'); echo $tmp[0]; ?></span>
                  <span class="sted"><?php $tmp = get_post_meta($post->ID, 'postnummer_sted'); echo $tmp[0]; ?></span>
                  <div class="links">
                    <?php 
                        $number = get_post_meta($post->ID, 'telefonnummer'); $number = $number[0];;
                        $number_formated = substr($number, 0, 3) . " " . substr($number, 3, 2) . " " . substr($number, 5, 3);
                     ?>
                    <a class="epost" href="mailto:<?php $tmp = get_post_meta($post->ID, 'epost'); echo $tmp[0]; ?>"><?php $tmp = get_post_meta($post->ID, 'epost'); echo $tmp[0]; ?></a>
                  </div>
                </address>
              </div>
            </div>

          <?php endwhile; ?>
      </div>
      
      <div class="orgfacts">
        <div class="row">
          <div class="small-12 columns">   
              <hr>
              <div>
                <span class="navn">Plan1 AS</span>
                <span class="postboks">Postboks <?php echo pods('instillinger_for_kontaktinfo')->field('postboks'); ?>, <?php $tmp = get_post_meta(36, 'postnummer_sted'); echo $tmp[0]; ?></span>
                <span class="orgnr">Org.nr.: <?php echo pods('instillinger_for_kontaktinfo')->field('organisasjonsnummer'); ?></span>
              </div>
          </div>
        </div>
      </div>


    </div>
<?php endif; ?>




</div> <!-- head-wrap -->




