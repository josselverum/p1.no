<?php $pod = pods('ansatte', array('orderby' => 'avdeling.post_title DESC', 'limit' => 999)); ?>
<?php while ($pod->fetch()): ?>

	<div class="medium-6 large-4 columns">
		<div class="ansatt clearfix">
			<div class="avatar"><?php echo get_the_post_thumbnail($pod->ID(), 'thumbnail'); ?></div>
			<h1 class="navn"><?php echo get_the_title($pod->ID()); ?></h1>
			<div class="stilling"><?php $tmp = get_post_meta($pod->ID(), 'stilling'); echo $tmp[0]; ?>, <?php $tmp = get_post_meta($pod->ID(), 'avdeling'); $tmp = $tmp[0]['post_title']; echo str_replace(array('Avdeling', 'Hovedkontor'), '', $tmp); ?></div>
			<div class="links">
				<?php if(is_mobile()): ?>
					<a href="tel:+47<?php $tmp = get_post_meta( $pod->ID(), 'telefonnummer'); $tmp = $tmp[0]; echo str_replace(' ', '', $tmp); ?>"><?php $tmp = get_post_meta( $pod->ID(), 'telefonnummer'); echo $tmp[0]; ?></a>
				<?php else: ?>
					<span><?php $tmp = get_post_meta( $pod->ID(), 'telefonnummer'); echo $tmp[0]; ?></span>
				<?php endif; ?>
				— <a href="mailto:<?php $tmp = get_post_meta( $pod->ID(), 'epost'); echo $tmp[0]; ?>"><?php $tmp = get_post_meta( $pod->ID(), 'epost'); echo $tmp[0]; ?></a>
			</div>
		</div>
	</div>

<?php endwhile; ?>