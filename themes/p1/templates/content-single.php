<div class="kunngjoring-archive">

<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(array('clearfix', 'kunngjoring')); ?>>
      <header>
        <?php get_template_part( 'templates/entry-meta' ); ?>
        <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
      </header>
      <div class="entry-content">
        <?php the_content(); ?>
      </div>
  </article>
<?php endwhile; ?>

</div>