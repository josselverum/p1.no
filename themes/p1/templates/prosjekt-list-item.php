
<article <?php post_class(array('prosjekt-list-item', 'clearfix')); ?>>
    <?php if ( has_post_thumbnail() and !is_singular( 'prosjekt' )): ?>
    <div class="project-list-image"><a href="<?php the_permalink(); ?>">
      <img
          data-interchange="[<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); echo $tmp[0]; ?>, (default)],
                    [<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'prosjekt_small' ); echo $tmp[0]; ?>, (medium)],
                    [<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'prosjekt_list_big' ); echo $tmp[0]; ?>, (xlarge)]
      ">
    </a> </div>



    <?php endif; ?>
  <div class="project-list-text">




<?php if(is_singular('prosjekt')): ?>

        <h1 class="prosjekt-title">
          <?php if(is_singular( 'prosjekt' )): ?>
            <?php the_title(); ?>
          <?php else: ?>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          <?php endif; ?>
        </h1>

<?php else: ?>


        <h2 class="prosjekt-title">
          <?php if(is_singular( 'prosjekt' )): ?>
            <?php the_title(); ?>
          <?php else: ?>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          <?php endif; ?>
        </h2>
<?php endif; ?>


        <div class="prosjekt-details">
          <?php if(get_post_meta($post->ID, 'oppdragsgiver', true)): ?>
            For <?php echo get_post_meta($post->ID, 'oppdragsgiver', true); ?>
          <?php endif; ?>
          <?php if(get_post_meta($post->ID, 'oppdragsgiver', true) and get_the_term_list( $post->ID, 'fagfelt')): ?>
            — 
          <?php endif; ?>
          <?php if(get_the_term_list($post->ID, 'fagfelt')): ?>
             <?php echo ucfirst(strtolower(strip_tags(get_the_term_list( $post->ID, 'fagfelt', '', ', ', '' )))); ?>
          <?php endif; ?> 
        </div>


        <?php if(is_post_type_archive('prosjekt')): ?>
          <div class="prosjekt-content show-for-large-up text-left">
            <div class="small-12 large-7 xlarge-12 large-centered columns">
              <?php the_content(); ?>
            </div>
          </div>
        <?php endif; ?>



        <?php if(is_front_page()): ?>
          <div class="prosjekt-content show-for-xlarge-up text-left">
            <div class="xlarge-12 columns">
              <?php the_content(); ?>
            </div>
          </div>
          <div class="row">
            <div class="medium-8 large-6 columns medium-centered text-centered">
                <a href="/prosjekter" class="button p1-button-dark">Se flere prosjekter</a>
              </div>
          </div>
        <?php endif; ?>


        <?php if(is_singular( 'prosjekt' )): ?>
          <div class="small-12 medium-8 medium-centered columns text-left">
            <?php the_content(); ?>
          </div>
        <?php endif; ?>
     </div>
  </article>