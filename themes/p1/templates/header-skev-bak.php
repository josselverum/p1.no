
<div id="menu-modal" class="reveal-modal full" data-reveal>

  <?php if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(array('theme_location' => 'primary_navigation'));
  endif; ?>

    <span class="p1-divider"></span>
    
    <div class="contact">
      <a href="tel:+47<?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>"><?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?></a>
      <a href="mailto:<?php echo pods('instillinger_for_kontaktinfo')->field('hovedepost'); ?>"><?php echo pods('instillinger_for_kontaktinfo')->field('hovedepost'); ?></a>
    </div>

  <a class="close-reveal-modal">&#215;</a>
</div>





  <header class="top">
    <div class="row">
      <div class="columns">
        <h1 class="left"><a href="<?php echo home_url(); ?>/"><img src="<?php bloginfo('template_directory'); ?>/assets/img_min/logo-wide-white.svg" alt="<?php bloginfo('name') ?>"></a></h1>
        <nav class="tab-bar right"><a class="menu-icon" data-reveal-id="menu-modal" href="#"><span></span></a></nav>
      </div>
    </div>
  </header>

  <?php if(is_archive()): ?>

  <div class="header-page-info">
    <div class="row">
      <div class="columns">

  <?php for ($i=3; $i <= 40; $i+=3) { 
    echo "<div class='slant2";
    if($i===3) { echo " firstslant";}
    echo "' style='width:" . $i . "em;'></div>";
  } ?>

    <p><?php $side = new WP_Query("page_id=25"); while($side->have_posts()) : $side->the_post();?><?php the_content(); ?><?php endwhile; ?></p>
      </div>
    </div>
  </div>

    <div class="header-page-info">
      <div class="row">
        <div class="columns">
        </div>
      </div>
    </div>
  <?php endif; ?>

  <!-- <span class="vinkel"></span> -->

