<div id="footer">
<div class="mainfooter">
	<footer class="row" role="contentinfo">
		<div class="small-12 columns">
		    

		    <?php if (has_nav_menu('footer_navigation')) : wp_nav_menu(array('theme_location' => 'footer_navigation')); endif; ?>

			<span class="p1-divider"></span>
		
			<div class="contact">
				<a href="tel:+47<?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>"><span itemprop="telephone"><?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?></span></a>
				<a href="mailto:<?php echo pods('instillinger_for_kontaktinfo')->field('hovedepost'); ?>"><span itemprop="email"><?php echo pods('instillinger_for_kontaktinfo')->field('hovedepost'); ?></span></a>
			</div>
			<span class="orgnr">Org.nr.: <?php echo pods('instillinger_for_kontaktinfo')->field('organisasjonsnummer'); ?></span>
			<span class="intranet"><a href="http://intranett.p1.no/">Intranett</a></span>



		</div>
	</footer>
</div>
</div>

</span> <!-- Google -->

<?php wp_footer(); ?>