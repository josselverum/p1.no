<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?> <?php if(is_post_type_archive('fagomrade')) {echo 'id="fixed-top"'; } ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

<?php
  do_action('get_header');
  get_template_part('templates/header');
?>




<?php if(is_front_page()): ?>


  <div class="prosjekt-block">
    <?php $page = new WP_Query(array('p' => 19, 'post_type' => 'any'));  $page->the_post();?>
    <?php get_template_part( 'templates/prosjekt-list-item' ); ?>
  </div>

  <div class="contact-block">
    <div class="row">
      <div class="medium-8 large-6 columns medium-centered text-centered">
        <span class="ring-oss-label text-center">Ring oss!</span>
        <h1 class="hovednummer">
          <?php if(is_mobile()): ?>
            <a href="tel:+47<?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>"><?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?></a>
          <?php else: ?>
            <?php echo pods('instillinger_for_kontaktinfo')->field('hovednummer'); ?>
          <?php endif; ?>
        </h1>
        <a href="/kontakt" class="button p1-button-dark">Kontakt en av oss!</a>
      </div>
    </div>
  </div>

  <div class="kunngjoring-block">
    <div class="kunngjoring-archive">
      <div class="row">
        <div class="small-12 medium-8 medium-centered columns">
          <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class(array('clearfix', 'kunngjoring')); ?>>
                <header>
                  <?php get_template_part( 'templates/entry-meta' ); ?>
                  <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </header>
                <div class="entry-excerpt show-for-medium-up">
                  <?php the_excerpt(); ?>
                </div>
            </article>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>

<?php else: ?>






  <?php if(is_post_type_archive('prosjekt') or is_page('kontakt') or is_post_type_archive('fagomrade') ): ?>
    <?php include roots_template_path(); ?>
  <?php else: ?>

    <div class="container" role="document">
      <div class="row" data-equalizer>
        <main class="main <?php echo roots_main_class(); ?>" role="main">
          <?php include roots_template_path(); ?>
        </main><!-- /.main -->
        <?php if (roots_display_sidebar()) : ?>
          <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
            <?php include roots_sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->

  <?php endif; ?>






<?php endif; ?>

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
