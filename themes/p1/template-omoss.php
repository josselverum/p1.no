<?php
/*
Template Name: Om oss
*/
?>





<div class="omoss">
		<div class="small-12 small-centered columns">
			<h1 class="ledergruppen-heading">Ledergruppen</h1>
			<h2 class="text-center">Daglig leder</h2>
			<?php $pod = pods('ansatte', pods('instillinger_for_kontaktinfo')->field('daglig_leder_id')); include(locate_template('templates/ansatt-kort.php')); ?>


		</div>
	
		<hr class="dark">

		<div class="small-12  small-centered columns">
			<h2 class="text-center">Teknisk leder</h2>
			<?php $pod = pods('ansatte', pods('instillinger_for_kontaktinfo')->field('teknisk_leder_id')); include(locate_template('templates/ansatt-kort.php')); ?>
		</div>

		<hr class="dark">


	<div class="clearfix">
		<div class="small-12 medium-4 columns">
			<h2 class="text-center">Avdelingsleder <br>Gardermoen</h2>
			<?php $pod = pods('ansatte', pods('instillinger_for_kontaktinfo')->field('avdelingsleder_gardermoen_id')); include(locate_template('templates/ansatt-kort.php')); ?>
		</div>
		<div class="small-12 medium-4 columns">
			<h2 class="text-center">Avdelingsleder <br>Hamar</h2>
			<?php $pod = pods('ansatte', pods('instillinger_for_kontaktinfo')->field('avdelingsleder_hamar_id')); include(locate_template('templates/ansatt-kort.php')); ?>
		</div>
		<div class="small-12 medium-4 columns">
			<h2 class="text-center">Leder <br>administrasjon</h2>
			<?php $pod = pods('ansatte', pods('instillinger_for_kontaktinfo')->field('leder_administrasjon_id')); include(locate_template('templates/ansatt-kort.php')); ?>
		</div>
	</div>

		<div class="show-for-medium-up"><hr class="dark"></div>

		<div class="medium-6 medium-centered columns"><a href="/kontakt" class="button p1-button-dark">Kontakt en av våre ansatte</a></div>
</div>