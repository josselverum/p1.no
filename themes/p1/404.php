<?php get_template_part('templates/page', 'header'); ?>

<h3>
  <?php _e('Kunne ikke finne siden du leter etter.', 'roots'); ?>
</h3>

<p><?php _e('Det kan være:', 'roots'); ?></p>
<ul>
  <li><?php _e('Du har skrevet nettadressen feil', 'roots'); ?></li>
  <li><?php _e('Lenken er gammel', 'roots'); ?></li>
</ul>

<?php get_search_form(); ?>
