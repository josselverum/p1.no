
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
var Roots = {
  // All pages
  common: {
    init: function() {
      // JavaScript to be fired on all pages
      $(document).foundation(); // Initialize foundation JS for all pages

      // Typekit
      (function(d) {var config = {kitId: 'rei0kqm', scriptTimeout: 3000 }, h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s) })(document);


      // Sticky footer
          $(window).bind("load", function () {
            var footer = $("#footer");
            var pos = footer.position();
            var height = $(window).height();
            height = height - pos.top;
            height = height - footer.height();
            if (height > 0) {
                footer.css({
                    'margin-top': height + 'px'
                });
            }
        });


        // Keypress intranett
        // $(document).keydown(function(e) {if(e.which == 73) {
          // window.location.replace("http://intranett.p1.no/"); 
        // } });

        
        // Scrollfunction
        $("a[href^='#']").on('click', function(e) { e.preventDefault(); var hash = this.hash; $('html, body').animate({ scrollTop: $(this.hash).offset().top - 0 }, 1000, function(){ window.location.hash = hash; }); });
        // Initiate skrollr
        // var s = skrollr.init();

        // Fjerner hover-effekt på bilder på touch
        if(Modernizr.touch) {
          $(".fagomrade-list-image img, .project-list-image img").css('opacity', '1');
        }


    }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on home page
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  },

  post_type_archive_fagomrade: {
    init: function() {
      


    }
  },

  kontakt: {
    init: function() {
      $("#uwpqsf_id_key").attr('placeholder', 'Finn en ansatt');
      // Auto process form 
      $('form#uwpqsffrom_71').bind("keyup",function() {process_data($(this)); });

    }
  }

};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
