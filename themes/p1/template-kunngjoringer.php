<?php
/*
Template Name: Kunngjøringer
*/
?>



<div class="kunngjoring-archive">
  <div class="small-12 medium-8 medium-centered columns">
    <?php $args = array('posts_per_page' => 10 ); $loop = new WP_Query( $args ); while ( $loop->have_posts() ) : $loop->the_post();?>

            <article <?php post_class(array('clearfix', 'kunngjoring')); ?>>
                <header>
                  <?php get_template_part( 'templates/entry-meta' ); ?>
                  <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                </header>
                <div class="entry-excerpt">
                  <?php the_excerpt(); ?>
                </div>
                <a class="lesmer" href="<?php the_permalink(); ?>">Les mer</a>
            </article>

    <?php endwhile; ?>
  </div>
</div>