

<?php if (!have_posts()) : ?>
  <p><?php _e('Sorry, no results were found.', 'roots'); ?> </p>
  <?php get_search_form(); ?>
<?php endif; ?>

<!-- <div class="header-page-image fagomrade-image"><?php echo wp_get_attachment_image(83, 'large'); ?></div> -->



<div class="fagomrader">
	<?php while( have_posts() ) : the_post(); //start of the loop ?>
	<?php if( $wp_query->current_post%2 == 0 ) echo "\n".'<div class="row" data-equalizer>'."\n"; ?>

	<div class="medium-6 columns">
		<article <?php post_class(); ?>>
		  <header>
		    <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		  </header>
		  <div class="entry-summary" data-equalizer-watch>
		    <?php the_excerpt(); ?>
		  </div>
		  <a href="<?php the_permalink(); ?>" class="button p1-button-dark">Mer om <?php the_title(); ?></a>
		</article>
	</div>

	<?php if( $wp_query->current_post%2 == 1 || $wp_query->current_post == $wp_query->post_count-1 ) echo '</div> <!--/.row-->'."\n"; ?>
	<?php endwhile; //end of the loop ?>
</div>