

<?php if (!have_posts()) : ?>
  <p><?php _e('Sorry, no results were found.', 'roots'); ?> </p>
  <?php get_search_form(); ?>
<?php endif; ?>



<div class="fagomrader">
	<?php while( have_posts() ) : the_post(); //start of the loop ?>
	<div class="fagomrade-list-item" id="<?php echo $post->post_name; ?>">



	<div class="fagomrade-list-image" >
      <img 
          data-interchange="[<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); echo $tmp[0]; ?>, (default)],
                    [<?php $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'prosjekt_small' ); echo $tmp[0]; ?>, (medium)],
      ">
    </div>





		<div class="row">
			<div class="small-12 medium-8 medium-centered large-7 large-centered columns">
				<article <?php post_class(); ?>>
				  <header>
				    <h1 class="entry-title"><?php the_title(); ?></h1>
				  </header>
				  <div class="entry-content">
				    <?php the_content(); ?>
				  </div>
				  <!-- <a href="<?php the_permalink(); ?>" class="button p1-button-dark">Mer om <?php the_title(); ?></a> -->
				</article>
			</div>
		</div>
	</div>
	<?php endwhile; //end of the loop ?>
</div>