<?php
/*
Plugin Name: Jöss AS - Branding, dashboard & cleaning
Plugin URI: http://www.joss.as
Description: Ads Jöss AS branding, dashboard widget and cleans the admin user interface
Version: 1.0
Author: Simen Schikulski
Author URI: http://www.joss.as
License: GPLv2
*/




// add a new logo to the login page, width 312px
function login_logo() { ?>
    <style type="text/css">
        .login #login h1 a {
            background-image: url( <?php echo plugins_url( '/logo.png' , __FILE__ ); ?> );
            background-size: 80%;
            height: 100px;
            width: 100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'login_logo' );





// add new dashboard widgets
function add_dashboard_widgets() {
    wp_add_dashboard_widget( 'dashboard_welcome', 'Velkommen', 'add_welcome_widget' );
}



function add_welcome_widget(){
	$greeting = Array(
		'Morn',
		'Hei og hå',
		'Tut-tut',
		'Hoi',
		'Shalom',
		'Tjenare',
		'Howdy'
	);

    $saying = Array(
    'Hva står på menyen idag?',
    'Ny sveis?',
    'Alt vel?',
    'Oppe og i farta?',
    'Ute og rusler?',
    'Takk for sist!',
    'Klar for arbe\'?'
);



?>


<!-- Trengs for å hente ut current user -->
 <?php global $current_user; get_currentuserinfo(); ?>   
 
    <h2><strong><?php echo $greeting[array_rand($greeting)]; ?>, <?php echo $current_user->user_firstname ?>!</strong> <?php echo $saying[array_rand($saying)]; ?> :)</h2>
  
    <ul>
        <li><strong><a href="/wp/wp-admin/edit.php">Endre/legge til kunngjøringer</a></strong></li>
        <li><strong><a href="/wp/wp-admin/edit.php?post_type=ansatte">Endre på ansatte</a></strong></li>
        <li><strong><a href="/wp/wp-admin/edit.php?post_type=prosjekt">Endre/legge til prosjekter</a></strong></li>
    </ul>
 
 	<hr style="margin: 30px 0">
    <p style="font-style: italic;"><img src="<?php echo plugins_url( '/logo.png' , __FILE__ ); ?>" style="width: 100px; height: auto; float: left; margin-right: 20px;">Trøbbel med nettsiden? Ta kontakt med <a href="mailto:simen@joss.as?subject=Trøbbel med nettsiden">Simen Schikulski</a> eller <a href="http://www.joss.as/about/" target="_blank">en av oss andre i Jöss!</a></p>
    <p>&nbsp;</p>

<?php } // function end
add_action( 'wp_dashboard_setup', 'add_dashboard_widgets' );